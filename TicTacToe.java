import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
        int row;
        int col;
        int round = 0;
        boolean game = true;
        printBoard(board);
        outer:
        while (game) {
            while (true) {
                System.out.print("Player 1 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                col = reader.nextInt();
                if (row < 1 || row > 3 || col < 1 || col > 3 || board[row - 1][col - 1] != ' ')
                    continue;
                else {
                    board[row - 1][col - 1] = 'X';
                    printBoard(board);
                    round++;


                }
                if (checkboard(board)) {
                    System.out.println("player 1 won");
                    break outer;

                }
                if (round == 9) {

                    System.out.println("draw");
                    break outer;


                }
                break;
            }

            while (true) {
                System.out.print("Player 2 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                col = reader.nextInt();

                if (row < 1 || row > 3 || col < 1 || col > 3 || board[row - 1][col - 1] != ' ')
                    continue;

                else {
                    board[row - 1][col - 1] = 'O';
                    round++;
                    printBoard(board);

                }

                if (checkboard(board)) {
                    System.out.println("player 2 won");
                    game = false;
                }

                if (round == 9) {
                    System.out.println("draw");
                    break outer;
                }

                break;
            }
        }

        reader.close();
    }

    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");

        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");

            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");

                if (col == 2)
                    System.out.print("|");

            }
            System.out.println();
            System.out.println("   -----------");

        }

    }

    public static boolean checkboard(char[][] board) {
        for (int i = 0; i <= 2; ++i) {
            if (board[i][0] == board[i][1] && board[i][0] == board[i][2] &&
                    board[i][0] != ' ' && board[i][1] != ' ' && board[i][0] != ' ' && board[i][2] != ' ')
                return true;
        }
        for (int i = 0; i <= 2; ++i) {
            if (board[0][i] == board[1][i] && board[0][i] == board[2][i] &&
                    board[0][i] != ' ' && board[1][i] != ' ' && board[0][i] != ' ' && board[2][i] != ' ')
                return true;
        }
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] &&
                board[0][0] != ' ' && board[1][1] != ' ' && board[1][1] != ' ' && board[2][2] != ' ')
            return true;

        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] &&
                board[0][2] != ' ' && board[1][1] != ' ' && board[1][1] != ' ' && board[2][0] != ' ')
            return true;
        return false;

    }

}